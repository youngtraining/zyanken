package janken;

/**
 * ジャンケンで出す手を決定するクラス
 */
public class DecideHand {
	// 出した手の種類を格納するリスト
	public static HandType[] handTypeList = new HandType[Player.playerNum];
	// それぞれの手の種類を出した人数
	public static int rockPlayer = 0;
	public static int scissorsPlayer = 0;
	public static int paperPlayer = 0;

	// 出す手の種類を定義
	public enum HandType {
		ROCK("グー"), SCISSORS("チョキ"), PAPER("パー");
		// 日本語の種類名
		String name;

		HandType(String name) {
			this.name = name;
		}
	}

	/**
	 * ジャンケンで出す手を決定するメソッド
	 */
	public static HandType[] decideHand() {
		// playerNum人分の手をランダムに決定
		for (int i = 0; i < Player.playerNum; i++) {
			int num = (int) (Math.random() * 3);

			HandType hand = null;
			if (num == 0) {
				hand = HandType.ROCK;
				rockPlayer = rockPlayer + 1;
			} else if (num == 1) {
				hand = HandType.SCISSORS;
				scissorsPlayer = scissorsPlayer + 1;
			} else if (num == 2) {
				hand = HandType.PAPER;
				paperPlayer = paperPlayer + 1;
			}
			handTypeList[i] = hand;
		}
		return handTypeList;
	}
}
