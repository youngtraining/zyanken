package janken;

/**
 * ゲーム結果を判定するクラス
 */
public class GameResult {
	// ゲーム結果
	public static String finalWinner = "";

	/**
	 * @param playerWinNum
	 *            各プレイヤーが勝った数
	 */
	public static String gameResult(int[] playerWinNum) {
		for (int i = Janken.gameNum; i > 0; i--) {
			if (finalWinner.equals("")) {
				for (int j = 0; j < Player.playerNum; j++) {
					if (Janken.playerWinNum[j] == i) {
						finalWinner = finalWinner + Player.playerNameList[j]
								+ " ";
					}
				}
			}
		}
		return finalWinner;
	}
}