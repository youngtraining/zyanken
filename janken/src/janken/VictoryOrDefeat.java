package janken;

import janken.DecideHand.HandType;

/**
 * ジャンケンの勝敗を判定するクラス
 */
public class VictoryOrDefeat {
	// 各ジャンケンの勝者
	public static String victoryOrDefeat = "";

	/**
	 * プレイヤーの人数に応じて勝敗判定のロジックを振り分けるメソッド
	 * 
	 * @param handTypeList
	 *            各プレイヤーが出した手のリスト
	 * @return
	 */
	public static String victoryOrDefeat(HandType[] handTypeList) {

		if (Player.playerNum == 2) {
			twoPlayers(handTypeList);
		} else if (Player.playerNum < 6) {
			manyPlayers(handTypeList);
		}
		return victoryOrDefeat;
	}

	/**
	 * プレイヤーが二人の場合の勝敗判定
	 * 
	 * @param hand
	 *            各プレイヤーが出した手のリスト
	 */
	private static void twoPlayers(HandType[] hand) {

		if (hand[0] == hand[1]) {
			victoryOrDefeat = "引き分けです";
		} else if ((hand[0] == HandType.ROCK && hand[1] == HandType.SCISSORS)
				|| (hand[0] == HandType.SCISSORS && hand[1] == HandType.PAPER)
				|| (hand[0] == HandType.PAPER && hand[1] == HandType.ROCK)) {
			Janken.playerWinNum[0] = Janken.playerWinNum[0] + 1;
			victoryOrDefeat = Player.playerNameList[0] + "さんが勝ちました！";
		} else {
			Janken.playerWinNum[1] = Janken.playerWinNum[1] + 1;
			victoryOrDefeat = Player.playerNameList[1] + "さんが勝ちました！";
		}
	}

	private static void manyPlayers(HandType[] hand) {
		if (DecideHand.rockPlayer != 0 && DecideHand.scissorsPlayer != 0
				&& DecideHand.paperPlayer != 0) {
			victoryOrDefeat = "引き分けです";
		} else if (DecideHand.rockPlayer == Player.playerNum
				|| DecideHand.scissorsPlayer == Player.playerNum
				|| DecideHand.paperPlayer == Player.playerNum) {
			victoryOrDefeat = "引き分けです";
		} else if (DecideHand.rockPlayer == 0) {
			for (int i = 0; i < Player.playerNum; i++) {
				if (DecideHand.handTypeList[i] == HandType.SCISSORS) {
					Janken.playerWinNum[i] = Janken.playerWinNum[i] + 1;
					victoryOrDefeat = victoryOrDefeat + " "
							+ Player.playerNameList[i];
				}
			}
			victoryOrDefeat = victoryOrDefeat + "さんが勝ちました！";
		} else if (DecideHand.scissorsPlayer == 0) {
			for (int i = 0; i < Player.playerNum; i++) {
				if (DecideHand.handTypeList[i] == HandType.PAPER) {
					Janken.playerWinNum[i] = Janken.playerWinNum[i] + 1;
					victoryOrDefeat = victoryOrDefeat + " "
							+ Player.playerNameList[i];
				}
			}
			victoryOrDefeat = victoryOrDefeat + "さんが勝ちました！";
		} else if (DecideHand.paperPlayer == 0) {
			for (int i = 0; i < Player.playerNum; i++) {
				if (DecideHand.handTypeList[i] == HandType.ROCK) {
					Janken.playerWinNum[i] = Janken.playerWinNum[i] + 1;
					victoryOrDefeat = victoryOrDefeat + " "
							+ Player.playerNameList[i];
				}
			}
			victoryOrDefeat = victoryOrDefeat + "さんが勝ちました！";
		}
	}

}
